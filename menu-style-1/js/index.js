/** JS iconbar */
function myFunction(x) {
    x.classList.toggle("change");
}
/** END JS iconbar*/
//-------JS sidenav---------//
/**Show sidenav */
function openNav() {
    $('#mySidenav').css({ transform: 'translateX(0)' });
    $('#mySidenav').css({ transition: 'all 0.5s' });
    // document.body.style.overflow = "hidden";
    $(document).ready(function() {
        $('#rotation').addClass('rotation');
        $('#rotation').removeClass('rotation_backwards');
    });
}
/**Hide sidenav */
function closeNav() {
    $('#mySidenav').css({ transform: 'translateX(100%)' });
    $('#mySidenav').css({ transition: 'all 0.5s' });
    document.body.style.overflow = "visible";
    $(document).ready(function() {
        $('#rotation').addClass(' rotation_backwards');
        $('#rotation').removeClass('rotation');
    });
}
/**Event click iconbar */
$(document).ready(function() {
    $("#hide-sidebar").click(function(event) {
        event.stopPropagation();
        if ($('#hide-sidebar').hasClass('change')) {
            openNav();
            $('#ovelay').addClass('ovelay');
        } else {
            closeNav();
            $('#ovelay').removeClass('ovelay');
        }
    });
    $("#mySidenav").click(function(event) {
        event.stopPropagation();
    });
    $("#close-btn").click(function(event) {
        closeNav();
        $('#ovelay').removeClass('ovelay');
    });
    $("body").click(function(event) {
        closeNav();
        $('#ovelay').removeClass('ovelay');
    });
});

$(document).ready(function() {
    /** Add button */
    var x = $(".sidenav ul li").find("ul");
    if (x.length != 0) {
        x.before('<button class="accordion"></button>')

    }
    /** Accodition */
    var acc_2 = document.getElementsByClassName("accordion");
    var j;

    for (j = 0; j < acc_2.length; j++) {
        acc_2[j].onclick = function() {
            this.classList.toggle("active");
            var sub_item = this.nextElementSibling;
            if (sub_item.style.maxHeight) {
                sub_item.style.maxHeight = null;
            } else {
                sub_item.style.maxHeight = sub_item.scrollHeight + 500 + "px";
            }
        }
    }
});