//--Them nut button--// 
var x = $(".hd_menu ul li").find("ul");
if (x.length != 0) {
    x.before('<i class="fa fa-caret-down accordion"></i>')
};

function openNav() {
    $('#menu-mobile').css({
        transform: 'translateX(0)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'hidden'
    });
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
}

$(document).ready(function() {

    var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "100px";
            }
        }
    }
});