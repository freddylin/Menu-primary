function openNav() {
    $('#menu-mobile').css({
        transform: 'translateX(0)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'hidden'
    });
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
}

$(document).ready(function() {
    var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
});



//slider
$('.slide-adv').owlCarousel({
    loop: true,
    nav: true,
    smartSpeed: 500,
    autoplay: true,
    autoplayTimeout: 2000,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

//slider
$('.slide_product').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    smartSpeed: 500,
    autoplay: true,
    autoplayTimeout: 3000,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});
$('.slide_product_2').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    smartSpeed: 500,
    autoplay: true,
    autoplayTimeout: 3000,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

//slider
$('.slide_caro').owlCarousel({
    loop: true,
    nav: true,
    smartSpeed: 500,
    autoplay: true,
    autoplayTimeout: 4000,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$('.owl-theme').owlCarousel({
    loop: true,
    nav: true,
    smartSpeed: 500,
    autoplay: true,
    autoplayTimeout: 4000,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0: {
            items: 2
        },
        480: {
            items: 4
        },
        1000: {
            items: 4
        }
    }
});

//--Them nut button--// 
var x = $(".sd_list ul li").find("ul");
if (x.length != 0) {
    x.before('<i class="fa fa-angle-right accordion"></i>')
    x.addClass('panel')
}

$('#grid_list .pro_content').hide();
$('#grid_list .pro_form_list').hide();

function viewGrid() {
    $('#grid_list .pro_content').hide();
    $('#grid_list .pro_form_list').hide();
    $('.btn-grid').addClass('active');
    $('.btn-list').removeClass('active');
    $('#grid_list').addClass('grid');
    $('#grid_list').removeClass('list');

}

function viewList() {
    $('#grid_list .pro_content').show();
    $('#grid_list .pro_form_list').show();
    $('.btn-list').addClass('active');
    $('.btn-grid').removeClass('active');
    $('#grid_list').addClass('list');
    $('#grid_list').removeClass('grid');
}

function numble_up() {
    var quantity = document.getElementById("numb").value;
    console.log(quantity);
    if (!isNaN(quantity) && quantity > 1) {
        quantity--;
        document.getElementById("numb").setAttribute("value", quantity);
    }

}

function numble_down() {
    var quantity = document.getElementById("numb").value;
    if (!isNaN(quantity)) {
        quantity++;
        document.getElementById("numb").setAttribute("value", quantity);
    }
}

//scrollup	
$(document).ready(function() {

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});