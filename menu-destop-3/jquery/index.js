function openNav() {
    $('#menu-mobile').css({
        transform: 'translateX(0)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'hidden'
    });
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
}

$(document).ready(function() {

    var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
});

//--Them nut button--// 
var x = $(".sub_menu ul li").find("ul");
if (x.length != 0) {
    x.before('<i class="fa fa-angle-right"></i>')

}

/* jquery search */

$(document).ready(function() {
    var kiemTra = 0;
    $(".hd_form").hide(400);
    $(".hd_search").click(function() {
        $(".hd_form").slideToggle(400);
        if (kiemTra == 0) {
            $(".hd_search a span").prepend("<span>Tắt </span>");
            kiemTra = 1;
            $(".hd_search a i").removeClass("fa-search");
            $(".hd_search a i").addClass("fa-times");

        } else {
            $(".hd_search a span span").remove();
            kiemTra = 0;
            $(".hd_search a i").removeClass("fa-times");
            $(".hd_search a i").addClass("fa-search");
        }
    });
});